from django.shortcuts import render

# Create your views here.

#from keyword allows importing of neccesary classes/moldues methods and other files needed in our applications from the django.http package while the import keyword defines what we are importing from the package
from django.http import HttpResponse

def index(request):
	return HttpResponse("Hello from the views.py file!")